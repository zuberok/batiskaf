﻿using UnityEngine;
using System.Collections;

public class Loading : MonoBehaviour {

	public static int levelToLoad = 1;

	void Start () {
        Time.timeScale = 1;
		StartCoroutine("LoadCoroutine");
	}
	
	IEnumerator LoadCoroutine(){
        
		yield return new WaitForSeconds(0.1f);
		Application.LoadLevelAsync(levelToLoad);
        
	}

	void OnApplicationQuit()
	{
		levelToLoad = 1;
	}
}

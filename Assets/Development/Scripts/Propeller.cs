﻿using UnityEngine;
using System.Collections;

public class Propeller : MonoBehaviour
{
    public int maxEmission;
    public int minEmission;
    public float bubbleDecreasePeriod;
    public ParticleSystem bubbles;
	private tk2dSpriteAnimator prop;
    public Boat boat;
    private void Awake()
    {
        boat = transform.parent.GetComponent<Boat>();
        prop = GetComponent<tk2dSpriteAnimator>();
    }

    public void Use()
    {
        bubbles.emissionRate = maxEmission;
        prop.Play();
        MusicThemeController.UseEngine();

    }

    public void Stop()
    {
        prop.Stop();
       DecreaseBubbles();
       MusicThemeController.StopEngine();
    }
    private void DecreaseBubbles()
    {
        bubbles.emissionRate--;
        if (bubbles.emissionRate > minEmission && !boat.isFlyingUp)
        {
            Invoke("DecreaseBubbles", bubbleDecreasePeriod);
        }
    }
}

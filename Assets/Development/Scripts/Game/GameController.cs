﻿using UnityEngine;
using System.Collections;


public class GameController : MonoBehaviour
{
    public GameObject gameOverPanel;
    public GameObject pauseButton;
    public float scoresPerX;

    public int scoreReefPeriod;
    private int reefCount;

    public UILabel scoreInfo;
    public UILabel finalScoreInfo;

	public GameObject[] reefPrefabs;
	public GameObject geyserPrefab;

	public Vector2 distBtwReefsRange;
	public Vector2 distBtwGeysersRange;

    public int scoreGeyserPeriod;

    public static int Score;
    private int geyserCount = 0;

    public int score
    {
        get { return Score; }
        set
        {
            if (value/scoreReefPeriod > reefCount)
            {
                reefCount = value/scoreReefPeriod;
                Barrier.CreateNew(reefPrefabs[UnityEngine.Random.Range(0, reefPrefabs.Length)]);
            }
            if (value / scoreGeyserPeriod > geyserCount)
            {
                geyserCount = value / scoreGeyserPeriod;
                Instantiate(geyserPrefab, transform.position + new Vector3(20f, -transform.position.y - 10f,-0.5f), geyserPrefab.transform.rotation);
            }
            if (value%100 == 45)
            {
                Informer.Show(value + 55f);
            }
            scoreInfo.text = finalScoreInfo.text = value.ToString();
            Score = value;
        }
    }

	GameObject _tempReef;
	GameObject _tempGeyser;
	public static GameObject[] frames;
	void Start () {
        Time.timeScale = 0;
		frames = new GameObject[] {GameObject.Find("Frame1"), GameObject.Find("Frame2")};
	    MusicThemeController.GameMusic();
	}

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Barrier"))
        {
            GameOver();
        }
    }

    void FixedUpdate()
    {
        if(score != Mathf.FloorToInt(scoresPerX * transform.position.x))
        {
            score = Mathf.FloorToInt(scoresPerX*transform.position.x);
            scoreInfo.text = finalScoreInfo.text = score.ToString();
        }
    }
	public void GameOver()
	{
        Destroy(gameObject);
		pauseButton.active = false;
		Time.timeScale = 0;
        finalScoreInfo.text = score.ToString();
		Highscore.CompareAndSaveResult(score);
		NGUITools.SetActive(gameOverPanel, true);
		MusicThemeController.Bang();
		MusicThemeController.MenuMusic();
        
        ScreenMove.use = false;
	}
}

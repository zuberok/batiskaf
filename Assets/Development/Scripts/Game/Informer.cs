﻿using UnityEngine;
using System.Collections;

public class Informer : MonoBehaviour
{
    public static GameObject table;
    public Vector3 InstPosition;
    private static Vector3 instPosition;
    void Awake()
    {
        table = gameObject;
        Debug.Log(table);
        instPosition = InstPosition;
        table.transform.Find("Label").GetComponent<UILabel>().text = "0";
    }

    public static void Show(float sign)
    {
        table.transform.Find("Label").GetComponent<UILabel>().text = sign.ToString();
        table.transform.localPosition = instPosition;
        
        Debug.LogWarning(table.transform.position);

    }

}

﻿using System.Net.NetworkInformation;
using UnityEngine;
using System.Collections;

public class ScreenMove : MonoBehaviour
{
    public static bool use;
    public float speed;
    private Vector3 _temp;
    public float deltaX;
    Transform bat;
    private void Start()
    {  
        bat = GameObject.Find("Batiskafe").transform;
    
    }

    void FixedUpdate ()
	{
        if(use)
        {
    	    _temp = transform.position;
    	    _temp.x += speed * Time.deltaTime;
    	    transform.position = _temp;
        }
	}

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            Debug.Log("Enter");
            _temp = transform.position;
            _temp.x += deltaX;
            Instantiate(gameObject, _temp, transform.rotation);
        }
    }

    public void OnBecameInvisible()
    {
    if (bat != null && transform.position.x < bat.position.x)
        {
            Destroy(gameObject);
        }
    }
}

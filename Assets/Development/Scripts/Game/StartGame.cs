﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour {

	void Update()
	{
		if (Input.GetMouseButtonDown(0) && gameObject.active)
		{
			gameObject.active = false;
			Time.timeScale = 1f;
		}
	}
}

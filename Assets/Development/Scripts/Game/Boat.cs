using System.Net.Mail;
using UnityEngine;
using System.Collections;

public class Boat : MonoBehaviour
{

    public static Transform boat;
    private Propeller propeller;
	public bool isFlyingUp;
    public float rotationValue;
    public float horizontalSpeed;
    public Vector2 speedsOfRotation;

    public Vector2 verticalSpeedRange;

	private Vector3 _nextPosition;
	private Vector3 _nextRotation;

    private float currentVerticalSpeed;
    private float _delta;

	void Start() {
		Debug.LogError("NEW BOAT");
		ScreenMove.use = true;
		isFlyingUp = true;
        propeller = transform.FindChild("propeller").GetComponent<Propeller>();
		_nextRotation = transform.eulerAngles;
		_nextPosition = transform.position;
	    boat = transform;
	}

	void Move() {	
		_nextPosition.y += currentVerticalSpeed * Time.deltaTime;
	    _nextPosition.x += horizontalSpeed*Time.deltaTime;
		if(isFlyingUp) {
			RisingToTheSurface();
		}
		else {
			Immersion();
		}
		transform.eulerAngles = _nextRotation;
		transform.position = _nextPosition;
	}

	void RisingToTheSurface()
	{
		_delta = speedsOfRotation.y;
		currentVerticalSpeed = Mathf.Clamp(currentVerticalSpeed + _delta, verticalSpeedRange.x, verticalSpeedRange.y);
		_nextRotation.z = currentVerticalSpeed * rotationValue;
	}

	void Immersion()
	{
		_delta = speedsOfRotation.x;
        currentVerticalSpeed = Mathf.Clamp(currentVerticalSpeed - _delta, verticalSpeedRange.x, verticalSpeedRange.y);
		_nextRotation.z = currentVerticalSpeed * rotationValue;
	}

    public void FixedUpdate()
    {
        Move();
    }

    void Update() {
		
		if(Input.GetMouseButtonDown(0) && Time.timeScale != 0)	
		{
			isFlyingUp = true;
            propeller.Use();
		}
		if(Input.GetMouseButtonUp(0)) 
		{
			isFlyingUp = false;
            propeller.Stop();
		}
	}
	void OnTriggerEnter(Collider col){
		if(col.CompareTag("Bomb") || col.CompareTag("Reef"))
		{
			GetComponent<GameController>().GameOver();
			
		}
	}
}

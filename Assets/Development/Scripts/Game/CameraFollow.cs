﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public Transform objectToFollow;
    public Vector3 distanceToObject;

    private Vector3 _temp;
    private void Update()
    {
        if (objectToFollow != null) 
        {
        	_temp = objectToFollow.position + distanceToObject;
	        _temp.y = 0;
	        transform.position = _temp;
	    }
    }
}

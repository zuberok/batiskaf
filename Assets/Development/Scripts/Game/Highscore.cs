﻿using UnityEngine;
using System.Collections;
using System;

public static class Highscore{

	static int[] GetScoreArray()
	{
		string[] oldScore = PlayerPrefs.GetString("Highscore").Split(); // вытаскиваем highscore из PlayerPrefs и раскидываем значения по массиву
		int[] result = new int[oldScore.Length];
		for(int i = 0; i < result.Length; ++i)
		{
			try {
				result[i] = int.Parse(oldScore[i]);
			}
			catch {
				 Debug.LogError("Can't parse highscore parameter. Check the value in PlayerPrefs");
			}
		}
		return result;
	}
	public static void CompareAndSaveResult(int _value)
	{
		if(!PlayerPrefs.HasKey("Highscore"))
		{
			PlayerPrefs.SetString("Highscore", "0 0 0 0 0");
		}
		int[] scoreArray = GetScoreArray();
		if(_value > scoreArray[0])	{
			SaveResult(_value, scoreArray);
		}
		
	}
	static void SaveResult(int _value, int[] _scoreArray)
	{
		_scoreArray[0] = _value;
		Array.Sort(_scoreArray);
		string result = string.Empty;
		for(int i = 0; i < _scoreArray.Length; ++i)
		{
			if(result != string.Empty)
			{
				result += " ";
			}
			result += _scoreArray[i];
		}
		PlayerPrefs.SetString("Highscore", result);
	}
}

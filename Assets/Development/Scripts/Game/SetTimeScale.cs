﻿using UnityEngine;
using System.Collections;

public class SetTimeScale : MonoBehaviour {

	public float _value;

	void OnClick()
	{
		Time.timeScale = _value;
	}
}

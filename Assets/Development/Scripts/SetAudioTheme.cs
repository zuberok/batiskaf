﻿using UnityEngine;
using System.Collections;

public class SetAudioTheme : MonoBehaviour {

	public enum Theme
	{
		Game,
		Menu,
	}
	public Theme audioTheme;
	void OnClick()
	{
		switch(audioTheme)
		{
			case Theme.Game:
				MusicThemeController.GameMusic();
				break;
			case Theme.Menu:
				MusicThemeController.MenuMusic();
				break;
		}
	}
}

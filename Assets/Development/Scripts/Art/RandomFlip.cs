﻿using UnityEngine;
using System.Collections;

public class RandomFlip : MonoBehaviour {
	private tk2dSpriteAnimator Anim;
	public float minFlipTime;
	public float maxFlipTime;
	void Start()
	{	
		Anim = GetComponent<tk2dSpriteAnimator> ();
		Invoke("Play",Random.Range(minFlipTime, maxFlipTime));
	}
	void Play()
	{

		Debug.Log("Flip");
		Anim.Play ("Flip");
		float delta = Random.Range(minFlipTime, maxFlipTime);
		Invoke("Play", delta);
			Debug.Log(gameObject);
	}
}

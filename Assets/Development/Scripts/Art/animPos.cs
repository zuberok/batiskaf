﻿using UnityEngine;
using System.Collections;

public class animPos : MonoBehaviour {

	public float xDelta;
	public float yDelta;
	public float zDelta;
	public Vector3 a;
	public Vector3 b;

	void Awake () {
		gameObject.GetComponent<TweenPosition> ().from = transform.position;
		a=transform.position;
		gameObject.GetComponent<TweenPosition> ().to = new Vector3 (transform.position.x + xDelta, transform.position.y+yDelta, transform.position.z);
		b= new Vector3 (transform.position.x + xDelta, transform.position.y+yDelta, transform.position.z+zDelta);
	}
	

}

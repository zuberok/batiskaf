﻿using UnityEngine;
using System.Collections;

public class MusicThemeController : MonoBehaviour {

	public AudioClip gameTheme;
	public AudioClip menuTheme;
	public AudioClip bangSound;
	static AudioClip BangSound;
	public AudioSource engineSound;
	static AudioClip GameTheme;
	static AudioClip MenuTheme;
	static AudioSource musicSource;
	static AudioSource EngineSound;
	public AudioSource bubbleSound;
	static AudioSource BubbleSound;

	void Awake()
	{
		BangSound = bangSound;
		GameTheme = gameTheme;
		MenuTheme = menuTheme;
		musicSource = GameObject.Find("MainTheme").GetComponent<AudioSource>();
		EngineSound = engineSound;
		BubbleSound = bubbleSound;
	}
	public static void MenuMusic()
	{
		musicSource.clip = MenuTheme;
		musicSource.Play();
	}
	public static void GameMusic()
	{
		musicSource.clip = GameTheme;
		musicSource.Play();
	}
	public static void UseEngine()
	{
		EngineSound.Play();
		Debug.Log(EngineSound.isPlaying);
		BubbleSound.Play();
	}
	public static void StopEngine()
	{
		EngineSound.Stop();
		Debug.Log(EngineSound.isPlaying);
		BubbleSound.Stop();
	}
	public static void Bang()
	{
		EngineSound.Stop();
		EngineSound.loop = false;	
		EngineSound.clip = BangSound;
		EngineSound.Play();
	}
}

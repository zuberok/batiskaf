﻿using UnityEngine;
using System.Collections;

public class ClosePanel : MonoBehaviour {

	public GameObject panel;
	void OnClick()
	{
		NGUITools.SetActive(panel, false);
	}
}

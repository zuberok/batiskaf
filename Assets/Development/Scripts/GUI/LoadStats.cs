﻿using UnityEngine;
using System.Collections.Generic;

public class LoadStats : MonoBehaviour {
    private List<UILabel> _positions;
	void Awake()
	{
        _positions = new List<UILabel>() {
                                                       {transform.Find("1st").GetComponent<UILabel>()},
                                                        {transform.Find("2nd").GetComponent<UILabel>()},
                                                        {transform.Find("3rd").GetComponent<UILabel>()},
                                                       {transform.Find("4th").GetComponent<UILabel>()},
                                                        {transform.Find("5th").GetComponent<UILabel>()} };
		string[] scores = PlayerPrefs.GetString("Highscore").Split();
        for (int i = 0; i < scores.Length; ++i)
        {
            _positions[i].text = scores[scores.Length - i - 1];
        }
	}
}

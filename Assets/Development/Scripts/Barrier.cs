﻿using UnityEngine;
using System.Collections;

public class Barrier : MonoBehaviour {

	static Vector3 reefInstPosition = new Vector3(55f,0,-0.5f);
    private static Vector3 _temp;
    public static void CreateNew(GameObject go)
    {
        _temp = Boat.boat.position + reefInstPosition;
        _temp.y = 0;
        Instantiate(go, _temp, go.transform.rotation);
    }
}

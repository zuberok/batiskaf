﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour
{
    private Vector3 _temp;
    public float speed;
    private void Update()
    {
        _temp = transform.eulerAngles;
        _temp.z += speed*Time.deltaTime;
        transform.eulerAngles = _temp;
    }
}
